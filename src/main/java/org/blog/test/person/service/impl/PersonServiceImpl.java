package org.blog.test.person.service.impl;

import org.blog.test.person.entity.Person;
import org.blog.test.person.repository.PersonRepository;
import org.blog.test.person.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public Person getPerson(Integer id) {
        return personRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Person getPersonFromSlave(Integer id) {
        return personRepository.findOne(id);
    }
}
