package org.blog.test.person.service;

import org.blog.test.person.entity.Person;
import org.springframework.transaction.annotation.Transactional;

public interface PersonService {

    Person getPerson(Integer id);

    @Transactional(readOnly = true)
    Person getPersonFromSlave(Integer id);
}
